package io.swagger.handler;

import io.swagger.inflector.models.RequestContext;
import io.swagger.inflector.models.ResponseContext;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import io.swagger.model.*;

import io.swagger.model.Request;
import io.swagger.model.RequestStatus;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaInflectorServerCodegen", date = "2017-09-10T11:00:24.083Z")
public class RequestController  {
  /** 
   * Uncomment and implement as you see fit.  These operations will map
   * Directly to operation calls from the routing logic.  Because the inflector
   * Code allows you to implement logic incrementally, they are disabled.
   **/
    private static HashMap<String, Object> results = new HashMap();
    private static HashMap<String, Request> pending = new HashMap();

    public ResponseContext addRequest(RequestContext request , Request command) {

        System.out.println(command.getCommand());

        String randomId = "random";
        while(results.containsKey(randomId) || pending.containsKey(randomId)) {
            randomId = new Random().nextInt()+"";
        }
        final String finalId = randomId;

        pending.put(finalId, command);

        /// you can run it in a thread and just return the finalID
        Runnable runnable = new Runnable() {
            public void run() {
                // here run the command
                // TODO command
                //and the store the results
                // like this results.put(finalId, <<RES>> );
                // pending.remove(finalId);
            }
        };

        // new Thread(runnable).start();

        return new ResponseContext().status(Status.CREATED).entity( finalId );

        //or run it here and return directly the results
        // TODO run command here
        // results.put(finalId, <<RES>> );
        // pending.remove(finalId);
        // return new ResponseContext().status(Status.OK).entity(<<RES>>);
    }



    public ResponseContext getRequestStatus(RequestContext request , String requestId) {
        if(results.containsKey(requestId)) {
            return new ResponseContext().status(Status.OK).entity( results.get(requestId) );
        } else if(pending.containsKey(requestId)) {
            return new ResponseContext().status(Status.FOUND).entity( pending.get(requestId) );
        } else {
            return new ResponseContext().status(Status.NOT_FOUND).entity( "Not found" );
        }
    }


}

